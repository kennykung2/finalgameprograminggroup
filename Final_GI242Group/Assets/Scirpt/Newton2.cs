using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Newton2 : MonoBehaviour
{
    public Vector3 Lastposition;
    public float acceleration;
  
    void Start()
    {
        Lastposition = transform.position;
    }

    void FixedUpdate()
    {
        GetComponent<Rigidbody>().AddForce(0, 0, 3 * Time.deltaTime + acceleration);
    }
}
