using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    void OnTriggerEnter (Collider other)
    {
        if(other.gameObject.name == "Deathfloor")
        {
            transform.position = new Vector3(0, 0, 0);
        }

        if (other.gameObject.name == "Deathfloor1")
        {
            transform.position = new Vector3(-2f, 9f, 41f);
        }

        if (other.gameObject.name == "Deathfloor2")
        {
            transform.position = new Vector3(0, 0, 145);
        }

    }
}
