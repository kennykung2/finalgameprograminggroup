using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCon : MonoBehaviour
{
    private CharacterController Playercon;

    public float Speed = 10;
    public Vector3 velocity;
    public float gravity;
    public float JumpHeight;

    void Start()
    {
        Playercon = GetComponent<CharacterController>();
    }

    void Update()
    {
        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        Playercon.Move(move * Time.deltaTime * Speed);

        if (move != Vector3.zero)
        {
            transform.forward = move;
        }

        velocity.y += gravity * Time.deltaTime;
        Playercon.Move(velocity * Time.deltaTime);

        if (Playercon.isGrounded && velocity.y < 0)
        {
            velocity.y = 10;
        }

        if(Input.GetKeyDown(KeyCode.Space) && Playercon.isGrounded)
        {
            velocity.y = Mathf.Sqrt(JumpHeight * -2 * gravity);
            velocity.y += JumpHeight;
        }
    }
}
