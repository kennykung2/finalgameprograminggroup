using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinText : MonoBehaviour
{
    public GameObject Wintext;
    void Start()
    {
        Wintext.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Wintext.SetActive(true);
        }
    }
}
