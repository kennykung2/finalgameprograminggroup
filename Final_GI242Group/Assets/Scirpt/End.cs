using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class End : MonoBehaviour
{
    public GameObject Endthis;

    private void OntriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Endthis.gameObject.SetActive(true);
        }
    }

    void Update()
    {
        
    }
}
