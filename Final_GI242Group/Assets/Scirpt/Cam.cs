using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam : MonoBehaviour
{
    public GameObject target;
    public float xView, yView, zView;

    void Update()
    {
        transform.position = target.transform.position + new Vector3(xView, yView, zView);
        transform.LookAt(target.transform.position);
    }
}
